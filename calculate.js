$.ajaxSetup({async: false});
var rpcUrl = "procedure.php";

function JsonRpcRequest(url, request, callback) {
    $.post(url, JSON.stringify(request), callback, "json");
}

function test(response) {
    if (response.result) {
        console.log(response.result);
    }
}

// сохраняет переданное значение в сессии
function addValue(property, value) {

    var request = {};

    request.method = "addValue";
    request.params = [property, value];
    request.id = 1;
    request.jsonrpc = "2.0";

    response = JsonRpcRequest(rpcUrl, request, test);

}

// запросить результат расчета
function calculate() {
    
    var request = {};
    
    request.method = "getCalculate";
    request.params = [];
    request.id = 1;
    request.jsonrpc = "2.0";

    $.post(rpcUrl, JSON.stringify(request), function (response) {
        console.log(response);
        if (response.result) {
            console.log(response.result);
            
            $('#rate_value').html(response.result.rate);
            $('#payment_value').html(response.result.payment);
            
            resultArea = document.getElementById("resultArea");
            resultArea.style.display = "block";

        }
    }, "json");

}

function resetStorage() {
    
    var request = {};
    
    request.method = "resetStorage";
    request.params = [];
    request.id = 1;
    request.jsonrpc = "2.0";

    $.post(rpcUrl, JSON.stringify(request), function (response) {
        
        if (response.result) {
            console.log('reset storage - ok');
        }
    }, "json");

}
