<?php

namespace Rsb;

class CreditRequest
{

    /**
     * Сумма кредита
     * @var int
     */
    private $sum;
    /**
     * Срок кредита, мес
     * @var int
     */
    private $period;
    /**
     * Надежный ли клиент
     * @var bool
     */
    private $reliable;
    /**
     * Бюджетная сфера
     * @var bool
     */
    private $budgetary;
    /**
     * Выбрано страхование
     * @var bool
     */
    private $insured;

    public function getSum(): int
    {
        return $this->sum;
    }

    public function getPeriod(): int
    {
        return $this->period;
    }

    public function getReliable(): bool
    {
        return $this->reliable;
    }

    public function getBudgetary(): bool
    {
        return $this->budgetary;
    }

    public function getInsured(): bool
    {
        return $this->insured;
    }

    public function setSum(int $sum)
    {
        $this->sum = $sum;
    }

    public function setPeriod(int $period)
    {
        $this->period = $period;
    }

    public function setReliable(bool $reliable)
    {
        $this->reliable = $reliable;
    }

    public function setBudgetary(bool $budgetary)
    {
        $this->budgetary = $budgetary;
    }

    public function setInsured(bool $insured)
    {
        $this->insured = $insured;
    }

    public function __get($name)
    {
        return $this->$name ?? false;
    }
}
