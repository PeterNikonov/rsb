<?php

namespace Rsb;

class CreditCalculator
{
    /**
     * @var CreditRequest
     */
    private $request;

    const PERIOD_RATE_1 = 27.4;
    const PERIOD_RATE_2 = 28.4;
    const PERIOD_RATE_3 = 29.4;

    /**
     * Описание условий при которых предоставляется скидка на процентную ставку
     * @var array
     */
    protected $discountConditions = [
        7 => ['reliable:+,budget:-,insured:-', 'reliable:+,budget:+,insured:-'],
        5 => ['reliable:-,budget:+,insured:-'],
        4.5 => ['reliable:-,budget:-,insured:+'],
        9.5 => ['reliable:-,budget:+,insured:+'],
        11.5 => ['reliable:+,budget:+,insured:+', 'reliable:+,budget:-,insured:+']
    ];

    public function __construct(CreditRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Базовая ставка, которая зависит от срока кредитования
     * @return float|int
     */
    private function getBaseRate()
    {

        $period1 = range(1, 12);
        $period2 = range(13, 36);
        $period3 = range(37, 60);

        if (in_array($this->request->getPeriod(), $period1)) {
            return self::PERIOD_RATE_1;
        }
        if (in_array($this->request->getPeriod(), $period2)) {
            return self::PERIOD_RATE_2;
        }
        if (in_array($this->request->getPeriod(), $period3)) {
            return self::PERIOD_RATE_3;
        }
    }

    /**
     * Определить совпадение данных в заявке с существующими условиями получения скидки на кредит
     * @return float|int
     */
    private function getPossibleDiscount()
    {
        foreach ($this->discountConditions as $percent => $conditions) {
            foreach ($conditions as $condition) {
                $properties = explode(',', $condition);
                $resolveCounter = 0;
                foreach ($properties as $property) {
                    list($optionName, $optionValue) = explode(':', $property);
                    if ($this->request->$optionName === ($optionValue === '+')) {
                        $resolveCounter++;
                    }
                }
                if ($resolveCounter == count($properties)) {
                    $possibleDiscount[] = $percent;
                }
            }
        }

        if (isset($possibleDiscount)) {
            return max($possibleDiscount);
        }

        return 0;
    }

    /**
     * Получить ставку для расчета
     * @return float|int
     */
    public function getRate()
    {
        return (floatval($this->getBaseRate()) - floatval($this->getPossibleDiscount()));
    }

    /**
     * Расчитать ежемесячный платеж
     * @return float
     */
    public function getMonthPayment()
    {
        $rate = $this->getRate();
        $rate = $rate / 100;
        $s = $this->request->getSum();
        $n = $this->request->getPeriod();
        $p = $rate / 12;

        $x = $s * ($p + ($p / (pow((1 + $p), $n) - 1)));

        return round($x, 2);
    }
}
