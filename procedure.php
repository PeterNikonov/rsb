<?php

session_start();
include './vendor/autoload.php';

use JsonRPC\Server;
use ObjectRpc\Procedure;
use ObjectRpc\Storage;
use Rsb\CreditRequest;
use Rsb\CreditCalculator;

$storage = new Storage('category');
$server = new Server();

// сохранить значения в сессию
$server->getProcedureHandler()->withObject(new Procedure($storage));

// сброс значений при перезагрузке формы
$server->getProcedureHandler()->withCallback("resetStorage", function() use ($storage) {
    $storage->dropInstance();
});

// получить расчет на основе данных, которые есть в сессии
$server->getProcedureHandler()->withCallback("getCalculate", function() use ($storage) {

    $creditRequest = new CreditRequest();
    
    if (isset($storage->getInstance()->period)) {
        $creditRequest->setPeriod($storage->getInstance()->period);
    }
    if (isset($storage->getInstance()->sum)) {
        $creditRequest->setSum($storage->getInstance()->sum);
    }
    if (isset($storage->getInstance()->budget)) {
        $creditRequest->setBudgetary($storage->getInstance()->budget);
    }
    if (isset($storage->getInstance()->reliable)) {
        $creditRequest->setReliable($storage->getInstance()->reliable);
    }
    if (isset($storage->getInstance()->insured)) {
        $creditRequest->setInsured($storage->getInstance()->insured);
    }
    
    $creditCalculator = new CreditCalculator($creditRequest);
    $rate = $creditCalculator->getRate();
    $payment = $creditCalculator->getMonthPayment();

    return array('rate' => $rate, 'payment' => $payment);

});

echo $server->execute();
